<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

$name       = $_POST['name']    ?? null;
$email      = $_POST['email']   ?? null;
$message    = $_POST['message'] ?? null;
$tel        = $_POST['tel']     ?? null;

	
$response = [
	"success" => true,
	"message" => null,
	"errors" => [],
];


if (!$name) {
	$response['success'] = false;
	$response['errors']['name'] = "Name is required";
}

if (!$email) {
	$response['success'] = false;
	$response['errors']['email'] = "Email is required";
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$response['success'] = false;
	$response['errors']['email'] = "Invalid email address";
}

if ($response['success']) {

    $mail = new PHPMailer(true);

    try {

        //Recipients
        $mail->setFrom('noreply@creedandculter.xyz', 'Website Information');
        $mail->addAddress('creedandculturexyz@gmail.com');     // Add a recipient
        $mail->addReplyTo('noreply@creedandculter.xyz', 'Website Information');
//        $mail->addCC('cc@example.com');

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "Website interest from: ".$name;
        $mail->Body    = '
        <p><strong>Name:</strong> '.$name.'</p>
        <p><strong>Email:</strong> '.$email.'</p>
        <p><strong>Tel:</strong> '.$tel.'</p>
        <p><strong>Message:</strong> '.nl2br($message).'</p>
        ';

        $mail->send();

        $response['success'] = true;
        $response['message'] = "Thank you for registering your interest in Creed and Culture.";

    } catch (Exception $e) {}
}


echo json_encode($response);
