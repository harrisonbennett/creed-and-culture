

/*!
 * Run a callback function after scrolling has stopped
 * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Function} callback The function to run after scrolling
 */
var scrollStop = function (callback) {

    // Make sure a valid callback was provided
    if (!callback || typeof callback !== 'function') return;

    // Setup scrolling variable
    var isScrolling;

    // Listen for scroll events
    window.addEventListener('scroll', function (event) {

        // Clear our timeout throughout the scroll
        window.clearTimeout(isScrolling);

        // Set a timeout to run after scrolling ends
        isScrolling = setTimeout(function() {

            // Run the callback
            callback();

        }, 66);

    }, false);

};


function makeTimer() {

    var endTime = new Date("20 April 2020 12:00:00 GMT+01:00");
    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $("#days").html(days);
    $("#hours").html(hours);
    $("#minutes").html(minutes);
    $("#seconds").html(seconds);

}


function expandTextarea(id) {
    document.getElementById(id).addEventListener('keyup', function() {
        this.style.overflow = 'hidden';
        this.style.height = 30;
        this.style.height = this.scrollHeight + 'px';
    }, false);
}

expandTextarea('message-field');



$( document ).ready(function() {

    scrollStop(function () {
        if ($('#notify-me-button').visible(true) == false) {
            // The element is visible, do something
            $('body').removeClass("arrow-hidden");
        } else {
            // The element is NOT visible, do something else
            $('body').addClass("arrow-hidden");
        }
    });

    if ($('#notify-me-button').visible(true) == false) {
        // The element is visible, do something
        $('body').removeClass("arrow-hidden");
    } else {
        // The element is NOT visible, do something else
        $('body').addClass("arrow-hidden");
    }


    new TypeIt("#notify-me-button", {
        // speed: 100,
        strings: "NOTIFY ME",
        waitUntilVisible: true,
        loop: true,
    }).options({speed: 100, deleteSpeed: 0}).go();


    setInterval(function () {
        makeTimer();
    }, 1000);


    $('#submit-form').click(function (e) {

        $('.form-group input, .form-group textarea').removeClass('is-invalid'); // remove the error class
        $('.form-group input, .form-group textarea').removeClass('is-valid'); // remove the error class
        $('.invalid-feedback').remove(); // remove the error text
        $('.alert').remove(); // remove the error text

        var formData = {
            'name': $('input[name=name]').val(),
            'email': $('input[name=email]').val(),
            'message': $('textarea[name=message]').val(),
            'tel': $('input[name=tel]').val()
        };

        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '/process.php', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        })
        // using the done promise callback
            .done(function (data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
                if (!data.success) {

                    // handle errors for name ---------------
                    if (data.errors.name) {
                        $('#name-group input').addClass('is-invalid'); // add the error class to show red input
                        $('#name-group').append('<div class="invalid-feedback">' + data.errors.name + '</div>'); // add the actual error message under our input
                    } else {
                        $('#name-group input').addClass('is-valid');
                    }

                    // handle errors for email ---------------
                    if (data.errors.email) {
                        $('#email-group input').addClass('is-invalid'); // add the error class to show red input
                        $('#email-group').append('<div class="invalid-feedback">' + data.errors.email + '</div>'); // add the actual error message under our input
                    } else {
                        $('#name-group input').addClass('is-valid');
                    }

                    // handle errors for tel  ---------------
                    if (data.errors.message) {
                        $('#tel-group input').addClass('is-invalid'); // add the error class to show red input
                        $('#tel-group').append('<div class="invalid-feedback">' + data.errors.tel + '</div>'); // add the actual error message under our input
                    } else {
                        $('#tel-group input').addClass('is-valid');
                    }
                    // handle errors for message  ---------------
                    if (data.errors.message) {
                        $('#message-group textarea').addClass('is-invalid'); // add the error class to show red input
                        $('#message-group').append('<div class="invalid-feedback">' + data.errors.message + '</div>'); // add the actual error message under our input
                    } else {
                        $('#message-group textarea').addClass('is-valid');
                    }

                } else {

                    $('#submit-form, .modal-title').hide();

                    // ALL GOOD! just sh
                    // ow the success message!
                    $('form .modal-body').html('<div class="alert alert-success"><strong>Success:</strong> ' + data.message + '</div>');


                    $('form').trigger("reset");

                    setTimeout(() => {

                        // $("#signupModal").modal("hide");

                        $('.form-group input, .form-group textarea').removeClass('is-invalid'); // remove the error class
                        $('.form-group input, .form-group textarea').removeClass('is-valid'); // remove the error class

                        $('.invalid-feedback').remove(); // remove the error text

                    }, 4000);

                }
            })

            // using the fail promise callback
            .fail(function (data) {

                // show any errors
                // best to remove for production
                console.log(data);
                $('form .modal-body').prepend('<div class="alert alert-danger"><strong>Error:</strong> Oops, something went wrong.</div>');
            });
    });
});